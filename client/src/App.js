import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends Component {
	constructor(props) {
		super(props);
		this.state= {
			projects:[]
		};
	}

componentDidMount() {
	this.gatherProjects();
}

gatherProjects = () => {
	axios.get('http://localhost:5555/api/projects')
		.then(response => {
			console.log (response)
			this.setState({ projects: response.data.projects });
		})
		.catch(error => console.log(error));
}
	
render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
 	<div>
		{this.state.projects.map(project => {
			return (
				<ul>
					<li>{project.name}: {project.description}</li>
				</ul>
			);
		})}
	</div>
      </div>
    );
  }
}

export default App;
