const express = require('express');
const cors = require('cors');
// helpers here
const actions = require('./data/helpers/actionModel.js');
const projects = require('./data/helpers/projectModel.js');

const port = 5555;
const server = express();
server.use(express.json());
server.use(cors({ origin: 'http://localhost:3000' }));

server.get('/', (req, res) => {
	res.send('Hello, Kelly');
})

//***************actionsCRUD*******************************

// I really struggled with the POST request for actions until I realized that contrary to the README description, the notes field IS required.

server.post('/api/actions', (req, res) => {
	if (!req.body.project_id || !req.body.description || !req.body.notes) {
		res.status(400);
		res.json({ errorMessage: "Please provide the project id, description, and notes for the action." });
	}
	else {
		const { project_id, description, notes } = req.body
		actions.insert({ project_id, description, notes })
			.then(response => {
				res.status(201);
				actions.get(response.id)
					.then(actions => {
						res.json({ actions });
					});
			})
			.catch(error => {
				res.status(500);
				res.json({ error: "There was an error saving the action to the database."});
				})
	}
})

server.get('/api/actions', (req, res) => {
	actions.get().then(actions => {
		res.json({ actions });
	})
		.catch(error => {
			res.status(500);
			res.json({ error: "The action information could not be retrieved."});
		})
});

server.get('/api/actions/:id', (req, res) => {
	const { id } = req.params;
	actions.get(req.params.id).then(actions => {
		if (actions) {
			res.json({ actions });
		}
		else {
			res.status(404);
			res.json({ message: " The action with the specified ID does not exist." });
		}
	})
		.catch(error => {
			res.status(500);
			res.json({ error: "The action information could not be retrieved." });
		})
});

server.put('/api/actions/:id', (req, res) => {
	const { project_id, description } = req.body;
	const id = req.params.id;

	if (!project_id || !description) {
		res.status(400);
		res.json({ errorMessage: "Please provide the project id and description." });
	}
	else {
		actions.update( id, { project_id, description } ).then(success => {
			if (success) {
				res.status(200);
				actions.get(id)
					.then(actions => {
						res.json({ actions });
			});
		}

			else {
				res.status(404);
				res.json({ message: "The action with the specified ID does not exist." });
			}
		}
		)
			.catch(error => {
				res.status(500);
				res.json({ error: "The action could not be found." });
			})
}
})


server.delete('/api/actions/:id', (req, res) => {
	const { id } = req.params

	actions.get(id)
		.then((action) => {
			let deletedAction = action
			actions.remove(id).then(success => {
				if (success) {
					res.status(200);
					res.json({ deletedAction });
				}
				else {
					res.status(404);
					res.json({ message: "The action with the specified ID does not exist." })
				}
		})
		.catch(error => {
			res.status(500);
			res.json({ error: "The action could not be removed." });
			})
		})
})

//*********************************projectsCRUD*********************************************************

server.post('/api/projects', (req,res) => {
	if (!req.body.name || !req.body.description) {
		res.status(400);
		res.json({ errorMessage: "Please provide the name and description for the project." });
	}
	else {

	const { name, description } = req.body;
	projects.insert({ name, description })
		.then(response => {
			res.status(201);
			projects.get(response.id)
				.then(projects => {
					res.json({ projects });
				});
		})
			.catch(error => {
				res.status(500);
				res.json({ error: "There was an error saving the project to the database."});
			})
	}
})

server.get('/api/projects', (req, res) => {
	projects.get().then(projects => {
		res.json({ projects });
	})
		.catch(error => {
			res.status(500);
			res.json({ error: "The project information could not be retrieved."});
		})
});

server.get('/api/projects/:id', (req, res) => {
	const { id } = req.params;
	projects.get(req.params.id).then(projects => {
		if (projects) {
			res.json({ projects });
		}
		else {
			res.status(404);
			res.json({ message: " The project with the specified ID does not exist." });
		}
	})
		.catch(error => {
			res.status(500);
			res.json({ error: "The project information could not be retrieved." });
		})
});

// retrieve a list of actions for project id

server.get('/api/projects/:id/actions', (req, res) => {
	const id = req.params.id;
	projects.getProjectActions(id)
		.then(actionList => {
			if (actionList.length) res.json ({ actionList });
			else {
				res.status(404);
				res.json({ errorMessage: "No actions found for this project." });
			}
		})
		.catch(error => {
			res.status(500);
			res.json({ errorMessage: "The actions could not be retrieved." });
		})
})

server.put('/api/projects/:id', (req, res) => {
	const { name, description } = req.body;
	const id = req.params.id;

	if (!name || !description) {
		res.status(400);
		res.json({ errorMessage: "Please provide the name and description." });
	}
	else {
		projects.update( id, { name, description } ).then(success => {
			if (success) {
				res.status(200);
				projects.get(id)
					.then(projects => {
						res.json({ projects });
			});
		}

			else {
				res.status(404);
				res.json({ message: "The project with the specified ID does not exist." });
			}
		}
		)
			.catch(error => {
				res.status(500);
				res.json({ error: "The project could not be found." });
			})
}
})


server.delete('/api/projects/:id', (req, res) => {
	const { id } = req.params

	projects.get(id)
		.then((project) => {
			let deletedProject = project
			projects.remove(id).then(success => {
				if (success) {
					res.status(200);
					res.json({ deletedProject });
				}
				else {
					res.status(404);
					res.json({ message: "The project with the specified ID does not exist." })
				}
		})
		.catch(error => {
			res.status(500);
			res.json({ error: "The project could not be removed." });
			})
		})
})


server.listen(port, () => console.log(`Server running on port ${port}`));
