# Review Questions

## What is Node.js? It lets us run javascript on a server

## What is Express? a framework for handling http requests, like CRUD methods and routes. This gives developers some important utilities for building the backend of web applications.

## Mention two parts of Express that you learned about this week - the homies (req and res) and built-in middleware.

## What is Middleware? a function that runs between request and response

## What is a Resource? data that are accessed and modified through express requests

## What can the API return to help clients know if a request was successful? a 200 status

## How can we partition our application into sub-applications? I could never get this working correctly but theoretically I think you could break your endpoints off into different routers.

## What is CORS and why do we need it? CORS stands for Cross Origin Resource Sharing and it allows developers to securely request information across domains.
